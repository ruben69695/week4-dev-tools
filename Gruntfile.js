module.exports = function (grunt) {
    require("time-grunt")(grunt);
    require("jit-grunt")(grunt, {
        useminPrepare: "grunt-usemin"
    });

    grunt.initConfig({

        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: "css",
                    src: ["*.scss"],
                    dest: "css",
                    ext: ".css"
                }]
            }
        },

        watch: {
            scripts: {
                files: [
                    "css/*.scss",
                ],
                tasks: [
                    "css",
                ],
                options: {
                    spawn: false
                }
            }
        },

        browserSync: {
            dev: {
                bsFiles: {  // browser files
                    src: [
                        "css/*.*",
                        "*.html",
                        "js/*.js",
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: "./" // Directorio base dónde corre el servidor
                    }
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [
                    {
                        expand: true,
                        cwd: "./",
                        src: "images/*.{png,gif,jpg,jpeg}",
                        dest: "dist/"
                    },
                ]
            }
        },

        copy: {
            html: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: "./",
                        src: "*.html",
                        dest: "dist"
                    },
                ]
            },
            fonts: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: "node_modules/open-iconic/font",
                        src: "fonts/*.*",
                        dest: "dist"
                    }
                ]
            }
        },

        clean: {
            build: {
                src: [
                    "dist/",
                ]
            }
        },

        cssmin: {
            dist: {} 
        },

        uglify: {
            dist: {}
        },

        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [
                    {
                        expand: true,
                        cwd: "dist",
                        src: ["*.html"],
                        dest: "dist"
                    },
                ]
            }
        },

        filerev: {
            options: {
                encoding: "utf8",
                algorithm: "md5",
                length: 20
            },
            release: {
                files: [
                    {
                        src: [
                            "dist/js/*.js",
                            "dist/css/*.css",
                        ]
                    }
                ]
            }
        },

        concat: {
            options: {
                separator: ";"
            },
            dist: {}
        },

        useminPrepare: {
            foo: {
                dest: "dist",
                src: [
                    "index.html",
                    "contact.html",
                    "reviews.html",
                    "terms.html"
                ]
            },
            options: {
                flow: {
                    steps: {
                        css: [
                            "cssmin",
                        ],
                        js: [
                            "uglify",
                        ]
                    },
                    post: {
                        css: [
                            {
                                name: "cssmin",
                                createConfig: function(context, block) {
                                    var generated = context.options.generated;
                                    generated.options = {
                                        keepSpecialComments: 0,
                                        rebase: false
                                    }
                                }
                            },
                        ]
                    }
                }
            }
        },

        usemin: {
            html: [
                "dist/index.html",
                "dist/contact.html",
                "dist/reviews.html",
                "dist/terms.html"
            ],
            options: {
                assetsDir: [
                    "dist",
                    "dist/css",
                    "dist/js",
                ]
            }
        }
        
        
    });

    // Register Tasks
    grunt.registerTask("css", ["sass"]);
    grunt.registerTask("default", ["browserSync", "watch"]);
    grunt.registerTask("img:compress", ["imagemin"]);
    grunt.registerTask("build", [
        "clean",
        "copy",
        "imagemin",
        "useminPrepare",
        "concat",
        "cssmin",
        "uglify",
        "filerev",
        "usemin",
        "htmlmin"
    ]);
};