// Activar que se muestren los tooltips al cargar la pagina
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('.carousel').carousel({
        interval: 3000
    });
});

// Suscripción al evento de apertura del modal
$("#suscriptionModal").on("show.bs.modal", function(args) {
    console.log("El modal de suscripción se esta abriendo...");
    
    // Habilitamos deshabilitamos el botón y lo cambiamos de color
    $("#btnSubscribe").prop("disabled", true);
    $("#btnSubscribe").removeClass("btn-book-primary");
    $("#btnSubscribe").addClass("btn-warning");
});

// Suscripción al evento una vez ya ha finalizado la apertura del modal
$("#suscriptionModal").on("shown.bs.modal", function(args) {
    console.log("El modal de suscripción se abrió.");
});

// Suscripción al evento de cierre del modal
$("#suscriptionModal").on("hide.bs.modal", function(args) {
    console.log("El modal de suscripción se esta cerrando...");

    // Habilitamos de nuevo el botón y lo devolvemos a su color original
    $("#btnSubscribe").prop("disabled", false);
    $("#btnSubscribe").removeClass("btn-warning");
    $("#btnSubscribe").addClass("btn-book-primary");
});

// Suscipción al evento una vez ya ha finalizado el cierre del modal
$("#suscriptionModal").on("hidden.bs.modal", function(args) {
    console.log("El modal de suscripción se cerro.");
});