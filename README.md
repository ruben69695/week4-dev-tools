# Especialización en desarrollo FullStack
Desarrollo FullStack por la universidad Austral, cursado en Coursera.

## Diseñando páginas web con Bootstrap 4 - Evaluación del proyecto - Módulo 4

### Ejercicios
1. Construir componentes de Bootstrap utilizando JQuery y Javascript
2. Crear CSS utilizando preprocesadores Sass y Less
3. Automatizar tareas con scripts de NPM
4. Preparar tu sitio para subirlo a un servidor



---

### Web sobre libros de programación
Una página web responsive sobre los mejores libros de programación y dónde poder adquirlos en España

### Para empezar

Para satisfacer las dependencias del proyecto, ejecute:
```bash
npm install
```

Para usar la automatización con grunt, necesita tener instalado los siguientes paquetes
```bash
# Si no lo tiene instalado, instalar grunt command line de forma global
npm install -g grunt-cli

# Para ejecutar tareas grunt que compilen los archivos scss de SASS, necesitará tener instalado Ruby. 
# Si tienes linux o macos ya está instalado, si tiene windows tendrá que descargarlo en el siguiente enlace: https://www.ruby-lang.org/en/ . Una vez descargado ejecutar el siguiente comando con el gestor de paquetes gem de Ruby:
gem install sass
```

Para probar ejecute grunt en la consola para hacer funcionar el servicio y que se le muestre la aplicación web
```bash
grunt
```

Para compilar todo el proyecto y generar el paquete de distribución, ejecute en la consola:
```bash
grunt build
```

### Tecnologías
- HTML / CSS / JS / SASS
- Bootstrap 4
- NodeJS
- GruntJS
- Visual Studio Code
- Git
- Bitbucket